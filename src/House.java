public class House {

    private Cat cat;
    private Dog dog;

    public void addAnimal(Cat cat) {
        if (dog == null) {
            this.cat = cat;
        }
    }

    public void addAnimal(Dog dog) {
        if (cat == null) {
            this.dog = dog;
        }
    }

    public Cat getCat() {
        return cat;
    }

    public Dog getDog() {
        return dog;
    }

    public void knocknock() {
        if(dog != null){
            dog.makeSound();
        }

        if(cat != null){
            cat.makeSound();
        }
    }
}
